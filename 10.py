
def is_prime(n):
    if n<2 : return False
    if n==2 : return True;
    if n % 2 == 0:
        return False
    i=2
    while i*i<=n:
        if n % i == 0:
            return False
        i+=1
    return True

max = 2000000
sum = 0
for i in range(max):
    if is_prime(i): sum+=i
print(sum)
