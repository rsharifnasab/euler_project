import math
def is_prime(n):
    if n<2 : return False
    if n==2 : return True;
    if n % 2 == 0:
        return False
    i=2
    while i*i<=n:
        if n % i == 0:
            return False
        i+=1
    return True

last = 1
print(is_prime(1))
for i in range(10001):
    print("i is :",i)
    while is_prime(last) == False:
        print(last," was not prime")
        last+=1
    last+=1
print(last-1)
